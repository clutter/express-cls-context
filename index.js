"use strict";

const cls = require("cls-hooked");

const name = "@parthr/express-cls-context";

function createNamespace() {
    return cls.createNamespace(name);
}

function getNamespace() {
    return cls.getNamespace(name);
}

function middleware(req, res, next) {
    let ns = getNamespace() || createNamespace();

    ns.run(() => next());
}

function setContext(key, value) {
    let ns = getNamespace();

    if (!ns) {
        throw new ReferenceError("Express CLS Context not available");
    }

    return ns.set(key, value);
}

function getContext(key) {
    let ns = getNamespace();

    if (!ns) {
        throw new ReferenceError("Express CLS Context not available");
    }

    return ns.get(key);
}

exports.getNamespace = getNamespace;
exports.middleware = middleware;
exports.setContext = setContext;
exports.getContext = getContext;
