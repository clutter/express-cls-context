/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/

"use strict";

const assert = require("assert");
/* jshint ignore:start */
const promisify = require("util").promisify;
const bluebird = require("bluebird");
/* jshint ignore:end */
const expressCls = require("../");

before(function (done) {
    done();
});

describe("ConstConfig", function () {
    it("should export express middleware", function (done) {
        assert.strictEqual(typeof expressCls.middleware, "function");
        assert.strictEqual(expressCls.middleware.length, 3);
        done();
    });
    it("should throw ReferenceError on get/set without cls-namespace", function (done) {
        assert.strictEqual(expressCls.getNamespace(), undefined);
        assert.throws(function () {
            expressCls.setContext("ying", "yang");
        }, ReferenceError);
        assert.throws(function () {
            expressCls.getContext("ying");
        }, ReferenceError);
        done();
    });
    it("should create cls-namespace on calling middleware", function (done) {
        assert.strictEqual(expressCls.getNamespace(), undefined);
        expressCls.middleware({}, {}, function () {
            assert.ok(expressCls.getNamespace());
            done();
        });
    });
    it("should set/get in cls-namespace", function (done) {
        function onTimerExpiry() {
            assert.strictEqual(expressCls.getContext("ying"), "yang");
            done();
        }
        expressCls.middleware({}, {}, function () {
            expressCls.setContext("ying", "yang");
            setTimeout(onTimerExpiry, 10);
        });
    });
    it("should support multiple/independent contexts in cls-namespace", function (done) {
        var count = 0;

        function onTimer1Expiry() {
            count++;
            assert.strictEqual(expressCls.getContext("ying"), "yang");
            if (count === 2) {
                done();
            }
        }

        function onTimer2Expiry() {
            count++;
            assert.strictEqual(expressCls.getContext("ping"), "pong");
            if (count === 2) {
                done();
            }
        }

        expressCls.middleware({}, {}, function () {
            expressCls.setContext("ying", "yang");
            setTimeout(onTimer1Expiry, 10);
        });
        expressCls.middleware({}, {}, function () {
            expressCls.setContext("ping", "pong");
            setTimeout(onTimer2Expiry, 10);
        });
    });
    /* jshint ignore:start */
    it("should support native promisify/async/await", function (done) {
        expressCls.middleware({}, {}, function () {
            const sleep = promisify(setTimeout);

            async function checkValue() {
                await sleep(10);
                assert.strictEqual(expressCls.getContext("ying"), "yang");
                done();
            }
            expressCls.setContext("ying", "yang");
            checkValue();
        });
    });
    it("should support bluebird async/await", function (done) {
        expressCls.middleware({}, {}, function () {
            async function checkValue() {
                await bluebird.delay(10);
                assert.strictEqual(expressCls.getContext("ying"), "yang");
                done();
            }
            expressCls.setContext("ying", "yang");
            checkValue();
        });
    });
    /* jshint ignore:end */
});

after(function (done) {
    done();
});
